package com.appacademy.blogreader;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Xml;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class BlogPostTask extends AsyncTask<Activity, Void, XmlPullParser> {
    private Activity activity;

    @Override
    protected XmlPullParser doInBackground(Activity... activities) {
        activity = activities[0];
        XmlPullParser xmlParser = null;
        int responseCode;

        try {
            URL blogFeedURL = new URL("http://www.reddit.com/.xml");

            HttpURLConnection connection = (HttpURLConnection) blogFeedURL.openConnection();
            connection.connect();
            responseCode = connection.getResponseCode();

            if(responseCode == HttpURLConnection.HTTP_OK) {
                xmlParser = RedditXmlParser.get().parse(connection.getInputStream());
            }
            RedditXmlParser.get().readFeed(xmlParser);
            connection.disconnect();
        }
        catch (MalformedURLException error) {
            Log.e("Blog Post Task", "Malformed URL: " + error);
        }
        catch (IOException error) {
            Log.e("Blog Post Task", "IOException: " + error);
        }

        return xmlParser;
    }

    @Override
    protected void onPostExecute(XmlPullParser xmlParser) {
        GridView gridView = (GridView)activity.findViewById(R.id.gridView);

        BlogPostAdapter adapter = new BlogPostAdapter(activity, RedditXmlParser.get().posts);
        gridView.setAdapter(adapter);
    }
}

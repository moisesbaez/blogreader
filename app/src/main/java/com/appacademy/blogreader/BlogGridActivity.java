package com.appacademy.blogreader;

import android.app.Activity;
import android.os.Bundle;
import android.widget.GridView;

public class BlogGridActivity extends Activity {

    protected GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);

        new BlogPostTask().execute(this);

    }
}

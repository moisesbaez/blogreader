package com.appacademy.blogreader;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    private ImageView image;
    private BlogPost post;

    public DownloadImageTask(ImageView image, BlogPost post) {
        this.image = image;
        this.post = post;
    }

    @Override
    protected Bitmap doInBackground(String... urls) {
        URL url;
        Bitmap bitmap = null;
        HttpURLConnection connection = null;

        try {
            url = new URL(urls[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            int responseCode = connection.getResponseCode();

            if(responseCode == HttpURLConnection.HTTP_OK) {
                bitmap = BitmapFactory.decodeStream(connection.getInputStream());
            }
        }
        catch(MalformedURLException error) {
            Log.e("DownloadImageTask", "MalformedURLException: " + error);
        }
        catch(IOException error) {
            Log.e("DownloadImageTask", "IOException: " + error);
        }
        finally {
            if(connection != null) {
                connection.disconnect();
            }
        }

        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        post.bitmap = bitmap;
        image.setImageBitmap(bitmap);
    }
}

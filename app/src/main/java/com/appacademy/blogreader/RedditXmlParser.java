package com.appacademy.blogreader;

import android.util.Log;
import android.util.Xml;
import android.widget.ProgressBar;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class RedditXmlParser {
    private static RedditXmlParser parser;
    public ArrayList<BlogPost> posts;

    private RedditXmlParser() {
        posts = new ArrayList<BlogPost>();
    }

    public static RedditXmlParser get() {
        if(parser == null) {
            parser = new RedditXmlParser();
        }
        return parser;
    }

    public XmlPullParser parse(InputStream inputStream) {
        XmlPullParser xmlParser = Xml.newPullParser();
        try {
            xmlParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            xmlParser.setInput(inputStream, null);
            xmlParser.nextTag();
        }
        catch (XmlPullParserException error) {
            Log.e("RedditXmlParser", "XmlPullParserException: " + error);
        }
        catch (IOException error) {
            Log.e("RedditXmlParser", "IOException: " + error);
        }

        return xmlParser;
    }

    public void readFeed(XmlPullParser xmlParser) {
        try {
            xmlParser.require(XmlPullParser.START_TAG, null, "rss");
            while (xmlParser.next() != XmlPullParser.END_TAG) {
                if(xmlParser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }

                String name = xmlParser.getName();

                if(name.equals("channel")) {
                    continue;
                }
                else if(name.equals("item")) {
                    posts.add(readPost(xmlParser));
                }
                else {
                    skip(xmlParser);
                }
            }
        }
        catch (XmlPullParserException error) {
            Log.e("RedditXmlParser", "XmlPullParserException: " + error);
        }
        catch (IOException error) {
            Log.e("RedditXmlParser", "IOException: " + error);
        }
    }

    private BlogPost readPost(XmlPullParser xmlParser) {
        String title = "";
        String url = "";
        String date = "";
        String thumbnail = "null";
        try {
            xmlParser.require(XmlPullParser.START_TAG, null, "item");

            while (xmlParser.nextTag() != XmlPullParser.END_TAG) {
                if(xmlParser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }

                String name = xmlParser.getName();

                if(name.equals("title")) {
                    title = readXmlTag(xmlParser, name);
                }
                else if(name.equals("link")) {
                    url = readXmlTag(xmlParser, name);
                }
                else if(name.equals("pubDate")) {
                    date = readXmlTag(xmlParser, name);
                }
                else if(name.equals("media:thumbnail")) {
                    thumbnail = readXmlTag(xmlParser, name);
                    thumbnail = xmlParser.getAttributeValue(null, "url");
                }
                else {
                    skip(xmlParser);
                }
            }
        }
        catch (XmlPullParserException error) {
            Log.e("RedditXmlParser", "XmlPullParserException: " + error);
        }
        catch (IOException error) {
            Log.e("RedditXmlParser", "IOException: " + error);
        }
        return new BlogPost(title, url, date, "", thumbnail);
    }

    private String readXmlTag(XmlPullParser xmlParser, String tag) {
        String text = "";

        try {
            xmlParser.require(XmlPullParser.START_TAG, null, tag);
            if (xmlParser.next() == XmlPullParser.TEXT) {
                text = xmlParser.getText();
                xmlParser.nextTag();
            }
            xmlParser.require(XmlPullParser.END_TAG, null, tag);
            return text;
        }
        catch (XmlPullParserException error) {
            Log.e("RedditXmlParser", "XmlPullParserException: " + error);
        }
        catch (IOException error) {
            Log.e("RedditXmlParser", "IOException: " + error);
        }

        return text;
    }

    private void skip(XmlPullParser xmlParser) {
        try {
            if (xmlParser.getEventType() != XmlPullParser.START_TAG) {
                throw new IllegalStateException();
            }
            int depth = 1;
            while (depth != 0) {
                switch(xmlParser.next()) {
                    case XmlPullParser.END_TAG:
                        depth--;
                        break;
                    case XmlPullParser.START_TAG:
                        depth++;
                        break;
                }
            }
        }
        catch (XmlPullParserException error) {
            Log.e("RedditXmlParser", "XmlPullParserException: " + error);
        }
        catch (IOException error) {
            Log.e("RedditXmlParser", "IOException: " + error);
        }

    }


}

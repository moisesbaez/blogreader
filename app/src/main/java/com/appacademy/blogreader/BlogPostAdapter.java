package com.appacademy.blogreader;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class BlogPostAdapter extends ArrayAdapter<BlogPost> {
    private Context context;

    public BlogPostAdapter(Context context, ArrayList<BlogPost> posts) {
        super(context, 0, posts);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BlogPost post = getItem(position);

        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.blog_grid_image, parent, false);
        }

        ImageView image = (ImageView)convertView.findViewById(R.id.imageView);

        if (post.bitmap == null && !post.thumbnail.equals("null")) {
            new DownloadImageTask(image, post).execute(post.thumbnail);
        }
        else if(post.bitmap == null) {
            post.bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);
            image.setImageBitmap(post.bitmap);
        }
        else {
            image.setImageBitmap(post.bitmap);
        }

        TextView text = (TextView)convertView.findViewById(R.id.gridPostTitle);
        text.setText(post.title);

        return convertView;
    }
}
